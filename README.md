This is the coding challange of the group project of Computer Vision SS22 at Technical University of Munich. The aim of this project is to implement the "Tour into the Picture" [Horry et.al] algorithm using the programming language MATLAB. 

**Given:** 2D RGB Image
**Aim:** Render a 3D simulation of the given Image.

**Needed Toolboxes:** Image Processing Toolbox

**Short Tutorial:** Upon starting main, the user is guided through the necessary tasks by an instruction display in the lower right corner of the GUI. An example image is displayed and can be changed using the image button in the upper left corner. To jump into the 3D-world, at least a vanishing point and the background rectangle have to be selected. It is recommended to go through the tasks as displayed by the buttons on the left hand side, top to bottom. Every step can be undone with the undo button located in the upper left of the app. An assisted foreground object segmentation algorithm guides the user by firstly marking the background area to be filled in, and afterwards selecting the foreground object to be put into the 3D-world. If the automatic solution, as presented by a green image overlay, does not meet the required precision, the user can select "add" or "subtract" to adjust the automatic mask or redraw completely by hand. By pressing "Launch into 3D", a second window displays the projected 3D-world, in which the user can navigate by using the tools on the left hand side.

**File Structure:**
1) **main.m**: Please run this file as it is to pop open the gui. Enjoy!
2) **Data**: This folder contains all the pictures that are used as input to the programm as well as the output pictures, which are created by the transformation from 3D world to a 2D RGB picture.
3) **Define the 3D world**: This folder contains all the functions to create the 3D world from a given image.
4) **Foreground objects**: This folder contains all the functions to cutting out the foreground objects from a given image and displaying them as 3D objects in the final simulation of the programm. The user-friendly interface displays the current task.
5) **Gui**: This folder contains all the code to the user-friendly graphical user interface, in which a user has the opportunity to select a vanishing point, a rear wall and foreground objects. After the selection, the program is ready to be launched into 3D! Within the simulation, there are buttons on the left hand side to navigate inside of the simulation. One can move towards the picture, change the camera positions in x,y and z axes in every direction. Right above the 3D world, there is also a toolbar, in which 3 buttons are available. One can easily save the configuration of the camera position and return back to this position whenever needed. One other option is to save the 3D simulation to create a 2D image of the current camera configuration. The saved 2D picture is then displayed on the right hand side underneath the buttons.

The contributors are as follows:
Maximilian Lüdecke,
Fritz Girke,
Leon Hecht,
Fatma Dirman,
Ali Oguz Can

Contact to authors: < maximilian.luedecke, fritz.girke, leon.hecht, fatma.dirman, alioguz.can > @tum.de
