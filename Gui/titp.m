classdef titp

    properties
        image;
        current_van;
        current_back;
        current_fore = {};
        action_cells = {};
        images = [];
        foreground_selections = [];
        vanishing_points = [];
        background_selections = [];
        helpe = false;
    end

    properties(Dependent)
        action_index
        
    end

    methods
        function action_index = get.action_index(obj)
            action_index = length(obj.action_cells);
        end
        function obj = set.current_van(obj, van_point)
            if ~isempty(obj.vanishing_points)
                delete(obj.current_van);
            end
            obj.current_van = van_point;
            bringToFront(obj.current_van)
            if ~obj.helpe
                obj.action_cells{end+1} = {1, van_point.Position};
                obj.vanishing_points = [obj.vanishing_points obj.action_index];
            end
        end

        function obj = set.current_back(obj, back_rec)
            if ~isempty(obj.background_selections)
                delete(obj.current_back);
            end
            obj.current_back = back_rec;
            if ~obj.helpe
                obj.action_cells{end+1} = {2, back_rec.Position};
                obj.background_selections = [obj.background_selections obj.action_index];
            end
        end

        function obj = set.current_fore(obj, fore_poly)
            % delete(obj.current_fore);
            obj.current_fore = fore_poly;
            
            if ~obj.helpe
                obj.action_cells{end+1} = {3, fore_poly};
                obj.foreground_selections = [obj.foreground_selections obj.action_index];
            end
        end

        function obj = set.image(obj, img)
%             if length(obj.images) > 1
%                 delete(obj.image);
%             end
            obj.image = img;
            if ~obj.helpe
                obj.action_cells{end+1} = {4, img};
                obj.images = [obj.images obj.action_index];
            end
        end



        function van = draw_van_point(obj, handle,listner)
            helper = obj.action_cells{obj.vanishing_points(end)};
            pos = helper{2};
            van = drawpoint(handle, "Color",'m', "Position", pos,'Deletable',false);
            addlistener(van,'MovingROI',listner);
            addlistener(van,'ROIMoved',listner);
        end

        function back = draw_bac_rec(obj, handle,listner)
            helper = obj.action_cells{obj.background_selections(end)};
            pos = helper{2};
            back = drawrectangle(handle, "Color",'blue', "Position", pos,'Deletable',false);
            addlistener(back,'MovingROI',listner);  
            addlistener(back,'ROIMoved',listner);
        end

        function fore = draw_for_pol(obj, handle)
            helper = obj.action_cells{obj.foreground_selections(end)};
            pos = helper{2};
            fore = drawpolygon(handle, "Color",'yellow', "Position", pos);
        end

        function obj = new_image(obj, handle, listner)
            helper = obj.action_cells{obj.images(end)};
            img = helper{2};
            handle.XLim = [0 width(img)];
            handle.YLim = [0 height(img)];
            imshow(img,'Parent', handle);
            obj.helpe = true;

            if ~isempty(obj.vanishing_points)
                %delete(obj.current_van)
                obj.current_van = obj.draw_van_point(handle, listner);
            end

            if ~isempty(obj.background_selections)
                %delete(obj.current_back);
                obj.current_back = obj.draw_bac_rec(handle, listner);
            end

%             if ~isempty(obj.foreground_selections)
%                 obj.current_fore = obj.draw_for_pol(handle);
%             end
            obj.helpe = false;
        end

        function obj = undo(obj, handle, listner)
            helper = obj.action_cells{end};
            if isempty(helper)
                return
            end
            obj.helpe = true;
            switch helper{1}
                case 1
                    if length(obj.vanishing_points) > 1
                        obj.vanishing_points(end) = [];                      
                        obj.current_van = draw_van_point(obj, handle, listner);         
                        
                    elseif length(obj.vanishing_points) == 1
                        obj.vanishing_points = [];
                        delete(obj.current_van);
                        
                    end
                    obj.action_cells(end) = [];
                case 2
                     if length(obj.background_selections) > 1
                        obj.background_selections(end) = [];
                        obj.current_back = draw_bac_rec(obj, handle, listner);

                    elseif length(obj.background_selections) == 1
                        obj.background_selections = [];
                        delete(obj.current_back);
                     end
                     obj.action_cells(end) = [];

                case 3
                    if length(obj.foreground_selections) > 1
                        obj.foreground_selections(end) = [];
                        hel = obj.action_cells{obj.foreground_selections(end)};
                        obj.current_fore = hel{2};
                        obj.action_cells(end) = [];
                        obj = obj.undo(handle, listner);
                    end

                case 4
                    if length(obj.images) > 1
                        obj.images(end) = [];                      
                        obj = new_image(obj, handle, listner);
                        img = obj.action_cells{obj.images(end)};
                        obj.helpe = true;
                        obj.image = img{2};
                        obj.action_cells(end) = [];
                    end
              

            end
             obj.helpe = false;
        end 
        
    end
end
