function [] = plot_foreground_objects(app, inner_rect, p_van, border_corners, p_trans, surface_dims)
% Load result parameter cell array
parameter_ca = app.tip.current_fore;

% unload paramter_ca
fg_obj_ca = parameter_ca{1};
fg_obj_alpha_ca = parameter_ca{2};
fg_obj_fullsize_sus_ca = parameter_ca{3};
fg_obj_sus_dir_ca = parameter_ca{4};

% iterate over each element
for index = 1:length(fg_obj_ca)

    %% calculate first guess suspension point
    % get size of foreground object array
    [fg_rows, fg_cols, ~] = size(fg_obj_ca{index});
    
    % try to use the middle point of the object at first
    % calculate geometric middle point

    % if the geometric middle point of the image is not in sus plane
    % use the 2nd best alternative and go with uppest/lowest/left/right pt.
    % depending on user-supplied suspensionplane
    % get the important row/column of pixels (attachment plane)
    % use image mask to find sus pixel
    switch true
        case (strcmp(fg_obj_sus_dir_ca{index}, 'left'))
            % define important row/col of pixels for sus
            sus_attach_plane = fg_obj_alpha_ca{index}(:, 1);
            sus_attach_plane = sus_attach_plane(:);
            % find all non zero pixels in sus plane
            [sus_plane, ~] = find(sus_attach_plane~=0);
            % calculate cropped pixel coords of sus pt
            first_cropped_sus = [sus_plane(1), 1];
            last_cropped_sus = [sus_plane(end), 1];

        case (strcmp(fg_obj_sus_dir_ca{index}, 'right'))
            sus_attach_plane = fg_obj_alpha_ca{index}(:, end);
            sus_attach_plane = sus_attach_plane(:);

            [sus_plane, ~] = find(sus_attach_plane~=0);

            first_cropped_sus = [sus_plane(1), fg_cols];
            last_cropped_sus = [sus_plane(end), fg_cols];


        case (strcmp(fg_obj_sus_dir_ca{index}, 'ceiling'))
            sus_attach_plane = fg_obj_alpha_ca{index}(1, :);
            sus_attach_plane = sus_attach_plane(:);

            [sus_plane, ~] = find(sus_attach_plane~=0);

            first_cropped_sus = [1, sus_plane(1)];
            last_cropped_sus = [1, sus_plane(end)];

        case (strcmp(fg_obj_sus_dir_ca{index}, 'floor'))
            sus_attach_plane = fg_obj_alpha_ca{index}(end, :);
            sus_attach_plane = sus_attach_plane(:);

            [sus_plane, ~] = find(sus_attach_plane~=0);

            first_cropped_sus = [fg_rows, sus_plane(1)];
            last_cropped_sus = [fg_rows, sus_plane(end)];
    end

    %% calculate (smart) full size pixel coords of sus point
    % define cropped image offset (left upper point of cropped image)
    row_offset = fg_obj_fullsize_sus_ca{index}(1);
    col_offset = fg_obj_fullsize_sus_ca{index}(2);
    % add cropped image offset to cropped sus pixel coords
    % to obtain fullsize pixel coords of suspoint
    first_sus = [first_cropped_sus(1) + row_offset, first_cropped_sus(2) + col_offset];
    last_sus = [last_cropped_sus(1) + row_offset, last_cropped_sus(2) + col_offset];

    % turn first suspoint into wanted format
    suspxcoords = [first_sus(2), first_sus(1)];
    
    [sus_world_coords, vertice, scale_factor] = image2worldcoords(suspxcoords,inner_rect,p_van,border_corners,p_trans, surface_dims);
    % define sus offset in cropped image
    cropped_sus_row_offset = first_cropped_sus(1);
    cropped_sus_col_offset = first_cropped_sus(2);
    % if the suspension point is not inside wanted vertice...
    if ~strcmp(vertice, fg_obj_sus_dir_ca{index})
    % choose the "last" pixel (right instead of left, down instead of up)
        % transform last pixel coords into wanted format
        suspxcoords = [last_sus(2), last_sus(1)];
        [sus_world_coords, ~, scale_factor] = image2worldcoords(suspxcoords,inner_rect,p_van,border_corners,p_trans, surface_dims);
        % redefine sus offset in cropped image
        cropped_sus_row_offset = last_cropped_sus(1);
        cropped_sus_col_offset = last_cropped_sus(2);
    end
    
    % transform sus point world coords in usefull format
    x_sus_wc = floor(sus_world_coords(1));
    y_sus_wc = floor(sus_world_coords(2));
    z_sus_wc = floor(sus_world_coords(3));
   
    %% Resize image according to scaling factor
    resized_image = imresize(fg_obj_ca{index}, (scale_factor));
    resized_mask = imresize(fg_obj_alpha_ca{index}, (scale_factor));

    % update cropped image size
    [fg_rows, fg_cols, ~] = size(resized_image);
    
    % calculate new sus point in image
    % just scale the factors and take the floor of it
    cropped_sus_row_offset = floor(cropped_sus_row_offset * (scale_factor));
    cropped_sus_col_offset = floor(cropped_sus_col_offset * (scale_factor));
    
    %% calculate world coords of all foreground object corners
    % ANNAHME: (0,0,0) ist links oben hinten
    % x > nach rechts, y > größer nach unten
    % z größer nach vorn
    switch true
        case (strcmp(fg_obj_sus_dir_ca{index}, 'left'))
            x_left_corners = x_sus_wc;
            x_right_corners = x_sus_wc + (fg_cols - 1);
            y_upper_corners = y_sus_wc - (cropped_sus_row_offset -1);
            y_lower_corners = y_sus_wc + (fg_rows - (cropped_sus_row_offset));
        case (strcmp(fg_obj_sus_dir_ca{index}, 'right'))
            x_right_corners = x_sus_wc;
            x_left_corners = x_sus_wc - (fg_cols - 1);
            y_upper_corners = y_sus_wc - (cropped_sus_row_offset -1);
            y_lower_corners = y_sus_wc + (fg_rows - (cropped_sus_row_offset));
        case (strcmp(fg_obj_sus_dir_ca{index}, 'ceiling'))
            x_left_corners = x_sus_wc - (cropped_sus_col_offset -1);
            x_right_corners = x_sus_wc + (fg_cols - (cropped_sus_col_offset));
            y_upper_corners = y_sus_wc;
            y_lower_corners = y_sus_wc + (fg_rows - 1);
        case (strcmp(fg_obj_sus_dir_ca{index}, 'floor'))
            x_left_corners = x_sus_wc - (cropped_sus_col_offset -1);
            x_right_corners = x_sus_wc + (fg_cols - (cropped_sus_col_offset));
            y_upper_corners = y_sus_wc - (fg_rows - 1);
            y_lower_corners = y_sus_wc;
    end

    % create matrices containing the cartesian pixel coords
    % use meshgrid function
    x_grid_input = x_left_corners:1:x_right_corners;
    y_grid_input = y_upper_corners:1:y_lower_corners;
    [x_data, y_data] = meshgrid(x_grid_input, y_grid_input);
    z_data = ones(size(x_data)) * z_sus_wc;

    %% display image using plot3 and alpha channel 
    % plot using warp object
    hold on;
    warp_object = warp(x_data, y_data, z_data, resized_image);
    % set Alpha Data on object
    % use special alphadata setting for warp
    set(warp_object, 'FaceAlpha',  'texturemap', 'AlphaDataMapping', 'none', 'AlphaData', resized_mask);
    % set axes parent 
    warp_object.Parent = app.Axes;
   
    
end
end