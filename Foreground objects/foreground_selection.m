function [parameter_ca, background_img] = foreground_selection(app, parameter_ca)

%% Das brauche ich von Fritz:
background_filling_algo = 'coherent';

%% initialize button variables 
% Reset app variables
app.susdirection = 'idle';
app.inputconfirmation = 'idle';
app.addsubtract = 'idle';
% Hide Buttons
app.Left.Visible = 'off';
app.Right.Visible = 'off';
app.Ceiling.Visible = 'off';
app.Floor.Visible = 'off';

%% Load and process parameter cell array
fg_obj_ca = parameter_ca{1};
fg_obj_alpha_ca = parameter_ca{2};
fg_obj_fullsize_sus_ca = parameter_ca{3};
fg_obj_sus_dir_ca = parameter_ca{4};

% use current background image as selection image
rgb_image = app.tip.image;
%% Draw polygon on standard zoom_axes and confirm
% set normal sized window as standard
while true
    roi_polygon = images.roi.Polygon.empty;
    % as long as the user has not confirmed the polygon...
    % Display user instruction
    app.EditField.Value = 'Draw background region to be filled in';
    while isempty(roi_polygon)
        roi_polygon = drawpolygon(app.UIAxes, 'Color', "yellow",'FaceAlpha', 0.2);
    end
    roi_polygon.Label = "Region to be filled in";
    % reset button variable
    app.confirm_button_active = true; app.HTML.Data = 'green';
    app.inputconfirmation = 'idle';
    % ask user for confirmation
    app.EditField.Value = 'Apply selection or use undo button';
    % wait for button input
    uiwait(app.UIFigure)
    app.confirm_button_active = false; app.HTML.Data = 'red';
    % read out button input
    if strcmp(app.inputconfirmation, 'confirm')
        app.inputconfirmation = 'idle';
        break;
    % if user pressed delete button... 
    elseif strcmp(app.inputconfirmation, 'delete')
        app.inputconfirmation = 'idle';
        delete(roi_polygon);

    % else
    % raise exception
    end
end

%% update background masks
% single object background mask
bg_mask = createMask(roi_polygon, rgb_image);
% cumulative background mask
% delete roi object
delete(roi_polygon);

%% crop the image and the binary mask to only show the foreground object
% crop image
cropped_fg_image = rgb_image(any(bg_mask, 2), :, :);
cropped_fg_image = cropped_fg_image(:, any(bg_mask, 1), :);

% crop fg_mask
cropped_fg_mask = bg_mask(any(bg_mask, 2), :);
cropped_fg_mask = cropped_fg_mask(:, any(bg_mask, 1));

%% display the cropped image onto new GUI zoom_axes
% set normal axes on invisible
app.UIAxes.Visible = 'off';
set(get(app.UIAxes, 'children'), 'Visible', 'off');
% set zoom_axes as visible (only now, to prevent old images from showing)
app.UIAxes_zoom.Visible = 'on';
% set new zoom_axes as default
axes(app.UIAxes_zoom);
% display grey checkerboard image on background of transparent regions

% make image fit borders
% set axes limits to center picture
% disable rahmen
app.UIAxes_4.Visible = 'off';
set(get(app.UIAxes_4, 'children'), 'Visible', 'off');
% get size
[plot_limit_row, plot_limit_col] = size(cropped_fg_mask);
% calculate checkerboard tile size
tile_size = ceil(max(plot_limit_row, plot_limit_col) / 8);
% create checkerboard image
check_mask = logical(checkerboard(tile_size));
% Turn checkerboard into gray image
check_image = (255 * repmat(uint8(check_mask), 1, 1, 3));
% turn checkerboard into gray
% check_image()
% Plot Checkerboard
imshow(check_image, 'Parent', app.UIAxes_zoom);
hold on;
% display cropped image on zoom_axes
h = imshow(cropped_fg_image, 'Parent', app.UIAxes_zoom);
set(h, 'AlphaData', cropped_fg_mask);

% make image fit borders
app.UIAxes_zoom.XLim = [0, plot_limit_col];
app.UIAxes_zoom.YLim = [0, plot_limit_row];

%% DEBUG

%% Ask user for suspension plane
% Display user instruction
app.EditField.Value = 'Define suspension plane of foreground object';
% Buttons auf sichtbar stellen
app.Left.Visible = 'on';
app.Right.Visible = 'on';
app.Floor.Visible = 'on';
app.Ceiling.Visible = 'on';
% Buttonvariable resetten
app.susdirection = 'idle';
% Auf Buttonpress warten 
uiwait(app.UIFigure);
% Buttonvariable auslesen
haengepunkt_richtung = app.susdirection;
% Buttonvariable resetten
app.susdirection = 'idle';
% Buttons auf unsichtbar schalten
app.Left.Visible = 'off';
app.Right.Visible = 'off';
app.Floor.Visible = 'off';
app.Ceiling.Visible = 'off';

%% store the crop information for later reconstruction of the image
% This is the index of the top left position of the zoomed window
% find first nonempty column
col_sum = sum(bg_mask, 1);
col_offset = find(col_sum~=0, 1, 'first') -1;

% Find first nonempty row
row_sum = sum(bg_mask,2);
row_offset = find(row_sum~=0 , 1, 'first') -1;

%% draw foreground polygon
while true
    % initialize empty polygon
    lazysnap_fg = images.roi.Polygon.empty;
    % Display user instruction
    app.EditField.Value = 'Draw polygon strictly inside foreground object';
    while isempty(lazysnap_fg)
        lazysnap_fg = drawpolygon("Color", [0 1 0], "FaceAlpha", 0.2);
    end
    % Display roi label
    lazysnap_fg.Label = 'Foreground object';
   
    % ask user for confirmation
    % Display user instruction
    app.EditField.Value = 'Apply Selection or use undo button';
    % Reset Button variable
    app.inputconfirmation = 'idle';
    app.confirm_button_active = true; app.HTML.Data = 'green';
    % wait for button input
    uiwait(app.UIFigure);
    % read out button input
    if strcmp(app.inputconfirmation, 'confirm')
        app.inputconfirmation = 'idle';
        app.confirm_button_active = false; app.HTML.Data = 'red';
        break;
    % if user pressed delete button... 
    elseif strcmp(app.inputconfirmation, 'delete')
        app.inputconfirmation = 'idle';
        app.confirm_button_active = false; app.HTML.Data = 'red';
        delete(lazysnap_fg);
    % else
    % raise exception
    end
end

%% Store all ROI data and calculate image masks
% create image masks
lazysnap_fg_mask = createMask(lazysnap_fg, cropped_fg_image);

% add transparent regions in cropped image to background mask
lazysnap_bg_mask = ~cropped_fg_mask;

%% perform final steps and grabcutting
% calculate superpixel matrix for cropped image
superpixel_matrix = superpixels(cropped_fg_image, 500);
% perform grabcutting
lazy_fg_mask = grabcut(cropped_fg_image, superpixel_matrix, cropped_fg_mask,lazysnap_fg_mask, lazysnap_bg_mask, 'MaximumIterations', 10);

%% Display created fg object mask and ask user for confirmation
% delete ROIs
delete(lazysnap_fg);

% Display lazysnapping result as colored image overlay
hold on;
overlay = imshow(lazysnap_fg_mask, [0 1 0]);
% ... using cropped binary mask as alpha channel
set(overlay, 'AlphaData', (lazy_fg_mask * 0.5));
hold off;

%% ask user if adding/subtracting mask is wanted
while true
    % Display user instruction
    app.EditField.Value = 'Apply selection or click add/delete to manually edit mask';
    app.confirm_button_active = true; app.HTML.Data = 'green';
    % Display Buttons
    % reset button variable
    app.addsubtract = 'idle';
    app.inputconfirmation = 'idle';
    app.Subtract.Visible = 'on';
    app.Add.Visible = 'on';
    %wait for input
    uiwait(app.UIFigure);
    
    app.Subtract.Visible = 'off';
    app.Add.Visible = 'off';

    % read out button input
    if strcmp(app.inputconfirmation, 'confirm')
        % reset button variables
        app.addsubtract = 'idle';
        app.inputconfirmation = 'idle';
        app.confirm_button_active = false; app.HTML.Data = 'red';
        % disable buttons
        app.Subtract.Visible = 'off';
        app.Add.Visible = 'off';
        % check if mask is empty
        if ~any(lazy_fg_mask, 'all')
            return
        end
        break;
    
    % if user pressed subtract button... 
    elseif strcmp(app.addsubtract, 'subtract')
        app.addsubtract = 'idle';
        app.confirm_button_active = false; app.HTML.Data = 'red';
        % draw subtraction polygon
        while true
            % initialize empty polygon
            subtract_fg = images.roi.Polygon.empty;
            % as long as the user has not confirmed the polygon...
            % disable the add / subtract buttons
            app.Subtract.Visible = 'off';
            app.Add.Visible = 'off';
            % Display user instruction
            app.EditField.Value = 'Draw region to subtracted from foreground object';
            while isempty(subtract_fg)
                subtract_fg = drawpolygon("Color", [1 0 0], "FaceAlpha", 0.2);
            end
            % Display roi label
            subtract_fg.Label = 'Subtract region';
            % ask user for confirmation
            % Display user instruction
            app.EditField.Value = 'Apply selection or use undo button';
            % reset button variable
            app.confirm_button_active = true; app.HTML.Data = 'green';
            app.inputconfirmation = 'idle';
            % wait for button input
            uiwait(app.UIFigure);           
            % read out button input
            if strcmp(app.inputconfirmation, 'confirm')
                app.inputconfirmation = 'idle';
                app.confirm_button_active = false; app.HTML.Data = 'red';
                % finally create fg_mask from manual polygon
                subtract_fg_mask = createMask(subtract_fg, cropped_fg_image);
                lazy_fg_mask(subtract_fg_mask) = false;
                % check if the mask is empty
                if (any(lazy_fg_mask, 'all'))
                    % use area opening to delete all but the largest connected region
                    % determine connected components
                    CC = bwconncomp(lazy_fg_mask, 8);
                    % determine largest object in image and only keep that
                    numPixels = cellfun(@numel,CC.PixelIdxList);
                    [~, idx] = max(numPixels);
                    % create empty lazysnap mask
                    lazy_fg_mask = false(size(lazy_fg_mask));
                    lazy_fg_mask(CC.PixelIdxList{idx}) = true;
                end
                % update image overlay
                delete(overlay);
                hold on;
                overlay = imshow(lazysnap_fg_mask , [0 1 0]);
                % ... using cropped binary mask as alpha channel
                set(overlay, 'AlphaData', (lazy_fg_mask * 0.5));
                hold off;
                delete(subtract_fg);
                break;
            % if user pressed delete button...
            elseif strcmp(app.inputconfirmation, 'delete')
                app.inputconfirmation = 'idle';
                app.confirm_button_active = false; app.HTML.Data = 'red';
                delete(subtract_fg);
            % else
            % raise exception
            end
        end
        
    elseif strcmp(app.addsubtract, 'add')
        app.inputconfirmation = 'idle';
        app.confirm_button_active = false; app.HTML.Data = 'red';
        % draw addition polygon
        while true
            % initialize empty polygon
            add_fg = images.roi.Polygon.empty;       
            % Display user instruction
            app.EditField.Value = 'Draw polygon to add to foreground object';
            while isempty(add_fg)
                add_fg = drawpolygon("Color", [0 1 0], "FaceAlpha", 0.2);
            end
            % Display roi label
            add_fg.Label = 'Add this region';
            % ask user for confirmation
            % Display user instruction
            app.EditField.Value = 'Confirm your choice';
            % reset button variable
            app.confirm_button_active = true; app.HTML.Data = 'green';
            app.inputconfirmation = 'idle';
            % wait for button input
            uiwait(app.UIFigure);
            % read out button input
            if strcmp(app.inputconfirmation, 'confirm')
                app.confirm_button_active = false; app.HTML.Data = 'red';
                app.inputconfirmation = 'idle';
                if (any(lazy_fg_mask, 'all'))
                    % use area opening to delete all but the largest connected region
                    % determine connected components
                    CC = bwconncomp(lazy_fg_mask, 8);
                    % determine largest object in image and only keep that
                    numPixels = cellfun(@numel,CC.PixelIdxList);
                    [~, idx] = max(numPixels);
                    % create empty lazysnap mask
                    lazy_fg_mask = false(size(lazy_fg_mask));
                    lazy_fg_mask(CC.PixelIdxList{idx}) = true;
                end
                % finally create fg_mask from manual polygon
                add_fg_mask = createMask(add_fg, cropped_fg_image);
                lazy_fg_mask(add_fg_mask) = true;
                % update image overlay
                delete(overlay);
                hold on;
                overlay = imshow(lazysnap_fg_mask , [0 1 0]);
                % ... using cropped binary mask as alpha channel
                set(overlay, 'AlphaData', (lazy_fg_mask * 0.5));
                hold off;
                delete(add_fg);
                break;
            % if user pressed delete button... 
            elseif strcmp(app.inputconfirmation, 'delete')
                app.inputconfirmation = 'idle';
                app.confirm_button_active = false; app.HTML.Data = 'red';
                delete(add_fg);
            % else
            % raise exception
            end
        end
    end
end
%% perform area opening and mask editing one final time
if (any(lazy_fg_mask, 'all'))
    % use area opening to delete all but the largest connected region
    % determine connected components
    CC = bwconncomp(lazy_fg_mask, 8);
    % determine largest object in image and only keep that
    numPixels = cellfun(@numel,CC.PixelIdxList);
    [~, idx] = max(numPixels);
    % create empty lazysnap mask
    lazy_fg_mask = false(size(lazy_fg_mask));
    lazy_fg_mask(CC.PixelIdxList{idx}) = true;
end
% edit out background mask from lazy_fg_mask
lazy_fg_mask(~cropped_fg_mask) = false;


%% cutout foreground object using the final mask
% crop image
final_fg_image = cropped_fg_image(any(lazy_fg_mask, 2), :, :);
final_fg_image = final_fg_image(:, any(lazy_fg_mask, 1), :);

% crop fg_mask
final_fg_mask = lazy_fg_mask(any(lazy_fg_mask, 2), :);
final_fg_mask = final_fg_mask(:, any(lazy_fg_mask, 1));

%% adjust crop information to further cropping due to grabcutting
% sum over rows and find first nonzero col
col_sum = sum(lazy_fg_mask, 1);
added_col_offset = find(col_sum~=0, 1, 'first');

% sum over cols and find first col that contains non zero element
row_sum = sum(lazy_fg_mask,2);
added_row_offset = find(row_sum~=0 , 1, 'first');

% add the offsets to obtain the final value of the left upper index
final_col_offset = col_offset + max(0, added_col_offset -1);
final_row_offset = row_offset + max(0, added_row_offset -1);

%% cut out foreground object according to lazysnapping mask and store it
% store image
fg_obj_ca{end +1} = final_fg_image;
% store mask / alpha channel 
fg_obj_alpha_ca{end +1} = final_fg_mask;

%% fill in background using coherent / exemplary inpaint
switch true
    case strcmp(background_filling_algo, 'exemplary')
        background_img = inpaintExemplar(rgb_image, bg_mask);
    case strcmp(background_filling_algo, 'coherent')
        background_img = inpaintCoherent(rgb_image, bg_mask);
    % otherwise
end

%% define aufhangepunkt in cropped image
% calculate final pixel indices of suspension point in full size picture
% this is the point at the top left of the cropped image
fullsize_sus_col  = 1 + final_col_offset;
fullsize_sus_row = 1 + final_row_offset;

% store fullsize pixel indices of suspension point
fg_obj_fullsize_sus_ca{end +1} = [fullsize_sus_row, fullsize_sus_col];

%% Store results in cell array format
% update sus_dir_ca
fg_obj_sus_dir_ca{end+1} = string(haengepunkt_richtung);
% update parameter_ca
parameter_ca = {fg_obj_ca, fg_obj_alpha_ca, fg_obj_fullsize_sus_ca, fg_obj_sus_dir_ca};

%% set zoomed zoom_axes as invisible (use GUI code from fritz)
app.UIAxes_zoom.Visible = 'off';
set(get(app.UIAxes_zoom, 'children'), 'Visible', 'off');

%% enable normal axes
set(get(app.UIAxes, 'children'), 'Visible', 'on');
app.UIAxes.Visible = 'on';

%% Disable all Buttons and reset button variables
app.Add.Visible = 'off';
app.Subtract.Visible = 'off';
app.Left.Visible = 'off';
app.Right.Visible = 'off';
app.Ceiling.Visible = 'off';
app.Floor.Visible = 'off';
app.inputconfirmation = 'idle';
app.addsubtract = 'idle';
app.susdirection = 'idle';
app.EditField.Value = '';
app.confirm_button_active = false; app.HTML.Data = 'red';
% Rahmen wieder on
app.UIAxes_4.Visible = 'on';
set(get(app.UIAxes_4, 'children'), 'Visible', 'on');

end