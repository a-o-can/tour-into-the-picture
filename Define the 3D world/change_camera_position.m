function change_camera_position(model, axis, degree)
    % This function translates the camera position by the given axis.
    % Example:
    %   change_camera_position(model, "y", 200)
    tmp = model.CameraPosition;
    x = tmp(1);
    y = tmp(2);
    z = tmp(3);
    if axis == "x"
        x = x + degree;
    elseif axis == "y"
        y = y + degree;
    elseif axis == "z"
        z = z + degree;
    end
    model.CameraPosition = [x y z];
end