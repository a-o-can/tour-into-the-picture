function back_to_bookmark(model, cameraPosition, cameraTarget, cemeraTilt, cameraViewAngle)
    model.CameraPosition = cameraPosition;
    model.CameraTarget = cameraTarget;
    model.CameraUpVector = cemeraTilt;
    model.CameraViewAngle = cameraViewAngle;
end