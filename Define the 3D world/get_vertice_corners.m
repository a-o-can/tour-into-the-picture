function [p, p_lr, p_tb, p_b] = get_vertice_corners(imgdim, corner, p_van)
% GET_VERTICE_CORNERS calculates the intersection between the lines 
% starting in the vanishing point and going through the inner rectangle
% corners and the larger of the image borders that is met outside the
% actual image space (plot for better understanding)
    if corner(1) < p_van(1) && corner(2) < p_van(2)
        % top left corner
        [m, n] = get_line_params(corner, p_van);
        x = -n / m;
        p_tb = [x 0];
        p_lr = [0 n];
        if n < 0 % if y(x=0) of line is above top image border
            p_b = p_tb;
            p = p_lr;
        else 
            p_b = p_lr;
            p = p_tb;
        end
    elseif corner(1) < p_van(1) && corner(2) > p_van(2)
        % bottom left corner
        [m, n] = get_line_params(corner, p_van);
        x = (imgdim(2) - n) / m;
        p_tb = [x imgdim(2)];
        p_lr = [0 n];
        if n > imgdim(2) % if y(x=0) of line is below bottom image border
            p_b = p_tb;
            p = p_lr;
        else 
            p_b = p_lr;
            p = p_tb;
        end
    elseif corner(1) > p_van(1) && corner(2) < p_van(2)
        % top right corner
        [m, n] = get_line_params(corner, p_van);
        y = m * imgdim(1) + n;
        x = -n / m;
        p_tb = [x 0];
        p_lr = [imgdim(1) y];
        if y < 0
            p_b = p_tb;
            p = p_lr;
        else
            p_b = p_lr;
            p = p_tb;
        end
    elseif corner(1) > p_van(1) && corner(2) > p_van(2)
        % bottom right corner
        [m, n] = get_line_params(corner, p_van);
        y = m * imgdim(1) + n;
        x = (imgdim(2) - n) / m;
        p_tb = [x imgdim(2)];
        p_lr = [imgdim(1) y];
        if y > imgdim(2)
            p_b = p_tb;
            p = p_lr;
        else 
            p_b = p_lr;
            p = p_tb;
        end
    end
end
