function [cameraPosition, cameraTarget, ...
          cemeraTilt, cameraViewAngle] = get_bookmark(model)
    cameraPosition = model.CameraPosition;
    cameraTarget = model.CameraTarget;
    cemeraTilt = model.CameraUpVector;
    cameraViewAngle = model.CameraViewAngle;
end