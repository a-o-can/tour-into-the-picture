function [plt,back_img_dims] = create_tip_plot(surfaces, background, plt, app, inner_rect,p_van,border_corners,p_trans, surface_dims)
%CREATE_TIP_PLOT Plots the rectified vertices into a 3d plot using the warp
%function and subsequetly rotating and translating the surface object into 
%the right positions.

back_img_dims = size(background);
%% Create and position 3d surfaces
dummy = figure('visible','off');
hold(plt,"on");

% foreground objects
plot_foreground_objects(app, inner_rect, p_van, border_corners, p_trans, surface_dims);

% background image
surf_back = warp(background);
surf_back.Parent = plt;

% left vertice
surf_l = warp(surfaces{1});
direction = [0 1 0];
rotate(surf_l, direction, 90)
surf_l.XData = ones(size(surf_l.XData));
% translate in z direction so that the surface starts at the x-plane
surf_l.ZData = surf_l.ZData - min(min(surf_l.ZData));
surf_l.Parent = plt;

% right vertice
surf_r = warp(surfaces{2});
direction = [0 1 0];
rotate(surf_r, direction, -90)
surf_r.XData = ones(size(surf_r.XData)) .* (back_img_dims(2) - 1);
surf_r.ZData = surf_r.ZData - min(min(surf_r.ZData));
surf_r.Parent = plt;

% top vertice
surf_t = warp(surfaces{3});
direction = [1 0 0];
rotate(surf_t, direction, -90)
surf_t.ZData = surf_t.ZData - min(min(surf_t.ZData));
surf_t.YData = ones(size(surf_t.YData));
surf_t.Parent = plt;

% bottom vertice
surf_b = warp(surfaces{4});
direction = [1 0 0];
rotate(surf_b, direction, 90)
surf_b.ZData = surf_b.ZData - min(min(surf_b.ZData));
surf_b.YData = ones(size(surf_b.YData)) .* (back_img_dims(1) - 1);
surf_b.Parent = plt;

%% Plot Properties

%view(plt,0,90)
camup(plt,[0 -1 0])
camproj(plt,'perspective')
% z-position of camera: 3-fache von der Tiefe vom Boden
z_position = 3 * abs(max(surf_t.ZData, [], 'all'));
% Setting the x & y position of camera & target to the same value
camtarget(plt,[floor(back_img_dims(2)/2),floor(back_img_dims(1)/2), 0]);
campos(plt,[floor(back_img_dims(2)/2),floor(back_img_dims(1)/2), z_position])
camva(plt,45)
close(dummy)
end

