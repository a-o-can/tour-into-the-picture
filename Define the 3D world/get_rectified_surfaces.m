function [surfaces, transforms, surface_dims] = get_rectified_surfaces(vertices,corners_fixed,corners_moving,rect_dims,varargin)
%GET_3D_SURFACES takes the vertice cutouts and their corners in the 
% rectangular matrix and fits a projective transformation so that the 
% vertices fit in a rectangular area with the size being defined by the
% height or width of the background image and the longest edge of the
% corresponding vertice.
% Returns the rectified surfaces and the projective transformations as cell
% arrays in the order {left, right, top, bottom}

%% Input Parser
parser = inputParser;
default_plot = false;
valid_plot = @(x) islogical(x);

addRequired(parser, 'vertices');
addRequired(parser, 'corners_fixed');
addRequired(parser, 'corners_moving');
addRequired(parser, 'rect_dims');
addOptional(parser, 'plot', default_plot, valid_plot);
parse(parser, vertices, corners_fixed, corners_moving, rect_dims, varargin{:});
plot = parser.Results.plot;

%% Compute projective transformations
% the process is explained once in detail for the left vertice but is
% applicable for all others too

% left vertice
v_l = vertices{1};
c_m_l = corners_moving{1};
c_f_l = corners_fixed{1};
r_l_dims = rect_dims{1};
t_l = fitgeotrans(c_m_l, c_f_l,"projective");
% apply the calculated transformation to the vertice image and cut to the
% calculated rectangular width and height
v_l_rect = imwarp(v_l,t_l, 'OutputView',imref2d([r_l_dims(2), r_l_dims(1)]));

% right vertice
v_r = vertices{2};
c_m_r = corners_moving{2};
c_f_r = corners_fixed{2};
r_r_dims = rect_dims{2};
t_r = fitgeotrans(c_m_r, c_f_r,"projective");
v_r_rect = imwarp(v_r,t_r, 'OutputView',imref2d([r_r_dims(2), r_r_dims(1)]));

% top vertice
v_t = vertices{3};
c_m_t = corners_moving{3};
c_f_t = corners_fixed{3};
r_t_dims = rect_dims{3};
t_t = fitgeotrans(c_m_t, c_f_t,"projective");
v_t_rect = imwarp(v_t,t_t, 'OutputView',imref2d([r_t_dims(2), r_t_dims(1)]));

% bottom vertice
v_b = vertices{4};
c_m_b = corners_moving{4};
c_f_b = corners_fixed{4};
r_b_dims = rect_dims{4};
t_b = fitgeotrans(c_m_b, c_f_b,"projective");
v_b_rect = imwarp(v_b,t_b, 'OutputView',imref2d([r_b_dims(2), r_b_dims(1)]));

%% Set dims before cutting
surface_dims = {size(v_l_rect), size(v_r_rect), size(v_t_rect), size(v_b_rect)};

% cut the empty areas from the surfaces
v_l_rect(~any(v_l_rect,[2,3]), :, :) = [];
v_l_rect(:, ~any(v_l_rect,[1,3]), :) = [];
v_r_rect(~any(v_r_rect,[2,3]), :, :) = [];
v_r_rect(:, ~any(v_r_rect,[1,3]), :) = [];
v_t_rect(~any(v_t_rect,[2,3]), :, :) = [];
v_t_rect(:, ~any(v_t_rect,[1,3]), :) = [];
v_b_rect(~any(v_b_rect,[2,3]), :, :) = [];
v_b_rect(:, ~any(v_b_rect,[1,3]), :) = [];

% debug plot for transformations
if plot
    figure
    subplot(3,3,2)
    imshow(v_t_rect)
    subplot(3,3,4)
    imshow(v_l_rect)
    subplot(3,3,6)
    imshow(v_r_rect)
    subplot(3,3,8)
    imshow(v_b_rect)
end
% save the transformations and rectangular output images in cell arrays
transforms = {t_l, t_r, t_t, t_b};
surfaces = {v_l_rect, v_r_rect, v_t_rect, v_b_rect};
end

