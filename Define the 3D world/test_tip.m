[img, map] = imread("Gui/metro-station.png");


%% Get the inner rectangle and vanishing point from user input
figure
imshow(img)
roi = drawrectangle;
van = drawpoint;
foreground_test_point = drawpoint;
p_van = van.Position;
pos = roi.Position;
foreground_test_point = foreground_test_point.Position;
close

% define the inner rectangle in the format [x1 y1; x2 y2]
inner_rect = [pos(1), pos(2); pos(1) + pos(3), pos(2) + pos(4)]

% cutout the inner rectangle from the full image
int_rect = round(inner_rect);
img_back = img(int_rect(1,2):int_rect(2,2), int_rect(1,1):int_rect(2,1), :);
% figure;
% imshow(img_back)

%% get the vertices cut out from the full image and their corners
[vertices,corners_fixed,corners_moving,rect_dims,border_corners] = get_vertices(img, inner_rect, p_van, 'do_plot', true);

%% rectify the vertices
[surfaces, transforms] = get_rectified_surfaces(vertices,corners_fixed,corners_moving,rect_dims,'plot', true);

%% place the foreground objects
surface_dims = {size(surfaces{1}),size(surfaces{2}),size(surfaces{3}),size(surfaces{4})};
foreground_test_point_world = image2worldcoords(foreground_test_point, inner_rect, p_van, border_corners, transforms, surface_dims);

%% create the 3d plot of background image and vertices
figure;
axs = gca;
plt = create_tip_plot(surfaces, img_back, axs);
%plot3(foreground_test_point_world(1),foreground_test_point_world(2),foreground_test_point_world(3),'o')