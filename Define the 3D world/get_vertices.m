function [vertices,corners_fixed,corners_moving,rect_dims,border_corners] = get_vertices(img,rect,p_van,varargin)
%GET_VERTICES takes the inner rectangle corner and the vanishing point to
%define the four vertices in the image. These are then extracted and
%returned as masked images with all pixels not belonging to a vertice set
%to zero. The vertices are not yet recangulat but have 4 defined corners
%that are returned as the cell array corners and describe the position of
%the vertices in the rectangular matrix, not in the original image.
%
%Cause Matlab is a shitshow when it comes to 'modern' features such as code
%completion the following abbreviations are used:
%   tl = top left
%   tr = top right
%   bl = bottom left
%   br = bottom right
%   p = point
%   v = vertice

imgdim = size(img);
% switch size vector to [x y] format 
imgdim = [imgdim(2) imgdim(1)];

%% Input Parser
parser = inputParser;
default_plot = false;
valid_plot = @(x) islogical(x);

addRequired(parser, 'img');
addRequired(parser, 'rect');
addRequired(parser, 'p_van')
addOptional(parser, 'do_plot', default_plot, valid_plot);
parse(parser, img, rect, p_van, varargin{:});
do_plot = parser.Results.do_plot;

%% Inner rectangle corners
% rect is in format [x1 y1; x2 y2] so bl and tr corner have to be
% calculated
tl_inner = rect(1,:);
tr_inner = [rect(2,1) rect(1,2)];
bl_inner = [rect(1,1) rect(2,2)];
br_inner = rect(2,:);


%% Calculate the corners of the vertices
% The outer corners of the vertices are calculated, see the documentation
% of the get_vertice_corners function for details
[p_tl, p_tl_lr, p_tl_tb, p_tlb] = get_vertice_corners(imgdim, tl_inner, p_van);
[p_tr, p_tr_lr, p_tr_tb, p_trb] = get_vertice_corners(imgdim, tr_inner, p_van);
[p_bl, p_bl_lr, p_bl_tb, p_blb] = get_vertice_corners(imgdim, bl_inner, p_van);
[p_br, p_br_lr, p_br_tb, p_brb] = get_vertice_corners(imgdim, br_inner, p_van);

% Debug Plot of lines separating the vertices
if do_plot
    inner_rect_plt_points = [tl_inner; tr_inner; br_inner; bl_inner; tl_inner];
    figure;
    imshow(img)
    hold on;
    line([p_van(1) p_tlb(1)], [p_van(2) p_tlb(2)], 'Color', 'red', 'LineWidth', 4)
    line([p_van(1) p_blb(1)], [p_van(2) p_blb(2)], 'Color', 'red', 'LineWidth', 4)
    line([p_van(1) p_trb(1)], [p_van(2) p_trb(2)], 'Color', 'red', 'LineWidth', 4)
    line([p_van(1) p_brb(1)], [p_van(2) p_brb(2)], 'Color', 'red', 'LineWidth', 4)
    line(inner_rect_plt_points(:,1), inner_rect_plt_points(:,2), 'Color', 'green', 'LineWidth', 4)
    hold off;
end

%% Get cutouts of the vertices
% left vertice
corners_l = [p_tl_lr; tl_inner; p_bl_lr; bl_inner];
[v_l, ~] = get_roi_cutout(img, corners_l);
% right vertice
corners_r = [tr_inner; p_tr_lr; br_inner; p_br_lr];
[v_r, ~] = get_roi_cutout(img, corners_r);
% top vertice
corners_t = [p_tl_tb; p_tr_tb; tl_inner; tr_inner; ];
[v_t, ~] = get_roi_cutout(img, corners_t);
% bottom vertice
corners_b = [bl_inner; br_inner; p_bl_tb; p_br_tb];
[v_b, ~] = get_roi_cutout(img, corners_b);

%% Get corners of vertices in cutout
% corners have the order [tl tr bl br]

% left vertice
v_l_size = size(v_l);
% find right side start and end rows of the vertice where its height is
% smaller than the matrix height
nonzero = find(v_l(:, end, 1));
% save the corners in a matrix to return later
p_b_tl = [find(v_l(1,:,1) == 0, 1, 'first') 0];
p_b_bl = [find(v_l(end,:,1) == 0, 1, 'first') v_l_size(1)];
corners_v_l = [p_b_tl; v_l_size(2) nonzero(1); p_b_bl; v_l_size(2) nonzero(end)];

% right vertice
v_r_size = size(v_r);
nonzero = find(v_r(:, 1, 1));
p_b_tr = [find(v_r(1,:,1) == 0, 1, 'last') 0];
p_b_br = [find(v_r(end,:,1) == 0, 1, 'last') v_r_size(1)];
corners_v_r = [0 nonzero(1); p_b_tr; 0 nonzero(end); p_b_br];

% top vertice
v_t_size = size(v_t);
nonzero = find(v_t(end, :, 1));
p_b_tl = [0 find(v_t(:,1,1) == 0, 1, 'first')];
p_b_tr = [v_t_size(2) find(v_t(:,end,1) == 0, 1, 'first')];
corners_v_t = [p_b_tl; p_b_tr; nonzero(1) v_t_size(1); nonzero(end) v_t_size(1)];

% bottom vertice
v_b_size = size(v_b);
nonzero = find(v_b(1, :, 1));
p_b_bl = [0 find(v_b(:,1,1) == 0, 1, 'last')];
p_b_br = [v_b_size(2) find(v_b(:,end,1) == 0, 1, 'last')];
corners_v_b = [nonzero(1) 0; nonzero(end) 0; p_b_bl; p_b_br];

%% Calculate Rectifying dimensions
height = ceil(abs(tl_inner(2) - bl_inner(2)));
width = ceil(abs(tl_inner(1) - tr_inner(1)));

% Calculate missing height/width and the corresponding points of the
% vertices for correct correspondece points to fit the projective
% tarnsformation
v_tl_depth = max([norm(p_tl_tb - tl_inner) norm(p_tl_lr - tl_inner)]);
v_tr_depth = max([norm(p_tr_tb - tr_inner) norm(p_tr_lr - tr_inner)]);
v_bl_depth = max([norm(p_bl_lr - bl_inner) norm(p_bl_tb - bl_inner)]);
v_br_depth = max([norm(p_br_lr - br_inner) norm(p_br_tb - br_inner)]);
max_depth = max([v_tl_depth, v_tr_depth, v_bl_depth, v_br_depth]);

% x/y coordinates of vertice fixed points
v_c_tl = norm(p_tlb - tl_inner);
v_c_bl = norm(p_blb - bl_inner);
v_c_tr = norm(p_trb - tr_inner);
v_c_br = norm(p_brb - br_inner);

v_l_fixed = [max_depth - v_c_tl, 0; max_depth 0; max_depth - v_c_bl, height; max_depth, height];
v_l_dims = round([max_depth height]);
v_r_fixed = [0 0; v_c_tr, 0; 0 height; v_c_br, height];
v_r_dims = round([max_depth height]);
v_t_fixed = [0, max_depth - v_c_tl; width, max_depth - v_c_tr; 0 max_depth; width max_depth];
v_t_dims = round([width max_depth]);
v_b_fixed = [0 0; width 0; 0 v_c_bl; width v_c_br];
v_b_dims = round([width max_depth]);



%% Set output
vertices = {v_l, v_r, v_t, v_b};
corners_moving = {corners_v_l, corners_v_r, corners_v_t, corners_v_b};
corners_fixed = {v_l_fixed, v_r_fixed, v_t_fixed, v_b_fixed};
rect_dims = {v_l_dims, v_r_dims, v_t_dims, v_b_dims};
border_corners = {p_tlb, p_trb, p_blb, p_brb};

%% Do debug plot
% debug plot of all cutouts and background image
if do_plot
    int_rect = round(rect);
    img_back = img(int_rect(1,2):int_rect(2,2), int_rect(1,1):int_rect(2,1), :);
    figure
    subplot(3,3,2);
    imshow(v_t)
    subplot(3,3,4);
    imshow(v_l)
    subplot(3,3,5);
    imshow(img_back)
    subplot(3,3,6);
    imshow(v_r)
    subplot(3,3,8);
    imshow(v_b)
end


end

