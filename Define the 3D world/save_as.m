function save_as(model, path, name, type)
    % This function saves the axes object as a file locally: Inverse
    % transformation to 2D
    % Example
    %   path =  pwd;
    %   name = simple-room-changed;
    %   type = "jpg"
    %   save_as(model, path, name, type);
    if path == "default"
        path = fullfile(pwd, "..", "Data");
    end
    save_path = fullfile(path, name + "." + type);
    exportgraphics(model, save_path);
end