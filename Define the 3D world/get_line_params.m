function [m, n] = get_line_params(p1, p2)
%GET_LINE_PARAMS Calculates the inciine and offset towards the (0,0) point
%of a line going through p1 and p2
    n = (p1(2)*p2(1) - p1(1)*p2(2)) / (p2(1) - p1(1));
    m = (p2(2) - p1(2)) / (p2(1) - p1(1));
end

