function rotate_figure(model, axis, degree)
    % This function rotates the image by the given axis.
    % Example:
    %   rotate_figure(model, "x", 30)
    tmp = model.CameraTarget;
    x = tmp(1);
    y = tmp(2);
    z = tmp(3);
    if axis == "x"
        x = x + degree;
    elseif axis == "y"
        y = y + degree;
    elseif axis == "z"
        z = z + degree;
    end
    model.CameraTarget = [x y z];
end