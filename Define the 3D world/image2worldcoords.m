function [world_coords, vertice, scale_factor] = image2worldcoords(image_point,inner_rect,p_van,border_corners,p_trans, surface_dims)
%IMAGE2WORLDCOORDS Transforms the given image coords into 3d world coords
%   Image coords are given in the format [x y] and world coords are
%   returned in the format [x y z]

%% Find out which vertice point lies in
vertice = 'none';
% Find out if it is background
between_coords = @(x,p1,p2) (x(1) > p1(1) && x(1) < p2(1)) && (x(2) > p1(2) && x(2) < p2(2));  
if between_coords(image_point,inner_rect(1,:), inner_rect(2,:))
    world_coords = [image_point - inner_rect(1,:), 0];
    return
end

[m_tl, n_tl] = get_line_params(border_corners{1}, inner_rect(1,:));
[m_tr, n_tr] = get_line_params(border_corners{2}, [inner_rect(2,1), inner_rect(1,2)]);
[m_bl, n_bl] = get_line_params(border_corners{3}, [inner_rect(1,1), inner_rect(2,2)]);
[m_br, n_br] = get_line_params(border_corners{4}, inner_rect(2,:));

is_below = @(p,m,n) p(2) > (m * p(1) + n);
if image_point(1) <= p_van(1) && image_point(2) <= p_van(2)
    if is_below(image_point, m_tl, n_tl)
        vertice = 'left';
    else
        vertice = 'top';
    end
elseif image_point(1) >= p_van(1) && image_point(2) <= p_van(2)
    if is_below(image_point, m_tr, n_tr)
        vertice = 'right';
    else
        vertice = 'top';
    end
elseif image_point(1) <= p_van(1) && image_point(2) >= p_van(2)
    if is_below(image_point, m_bl, n_bl)
        vertice = 'bottom';
    else
        vertice = 'left';
    end
elseif image_point(1) >= p_van(1) && image_point(2) >= p_van(2)
    if is_below(image_point, m_br, n_br)
        vertice = 'bottom';
    else
        vertice = 'right';
    end
end

%% Transform point depending on the vertice
scale_factor = 1;
inner_rect_dim = inner_rect(2,:) - inner_rect(1,:);
if strcmp(vertice,'left')
    p_b = border_corners{1};
    offset = [0 p_b(2)];
    v_p = image_point - offset;
    v_d = surface_dims{1};
    [v_rx, v_ry] = transformPointsForward(p_trans{1}, v_p(1), v_p(2));
    world_coords = [0, v_ry, v_d(2)-v_rx];
    % calculate scale factor
    y1 = m_tl * image_point(1) + n_tl;
    y2 = m_bl * image_point(1) + n_bl;
    scale_factor = inner_rect_dim(2) / abs(y1 - y2);
elseif strcmp(vertice,'right')
    p_b = border_corners{2};
    offset = [inner_rect(2,1) p_b(2)];
    v_p = image_point - offset;
    [v_rx, v_ry] = transformPointsForward(p_trans{2}, v_p(1), v_p(2));
    world_coords = [inner_rect_dim(1), v_ry, v_rx];
    % calculate scale factor
    y1 = m_tr * image_point(1) + n_tr;
    y2 = m_br * image_point(1) + n_br;
    scale_factor = inner_rect_dim(2) / abs(y1 - y2);
elseif strcmp(vertice,'top')
    p_b = border_corners{1};
    offset = [p_b(1) 0];
    v_p = image_point - offset;
    v_d = surface_dims{3};
    [v_rx, v_ry] = transformPointsForward(p_trans{3}, v_p(1), v_p(2));
    world_coords = [v_rx, 0, v_d(1)-v_ry];
    % calculate scale factor
    x1 = (image_point(2) - n_tl) / m_tl;
    x2 = (image_point(2) - n_tr) / m_tr;
    scale_factor = inner_rect_dim(1) / abs(x1 - x2);
elseif strcmp(vertice,'bottom')
    p_b = border_corners{3};
    offset = [p_b(1) inner_rect(2,2)];
    v_p = image_point - offset;
    [v_rx, v_ry] = transformPointsForward(p_trans{4}, v_p(1), v_p(2));
    world_coords = [v_rx, inner_rect_dim(2), v_ry];
    % calculate scale factor
    x1 = (image_point(2) - n_bl) / m_bl;
    x2 = (image_point(2) - n_br) / m_br;
    scale_factor = inner_rect_dim(1) / abs(x1 - x2);
end
end

