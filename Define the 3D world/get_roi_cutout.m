function [cutout, mask] = get_roi_cutout(img, corners)
% GET_ROI_CUTOUT returns a rectangular matrix containing the region of
% interest with height and width being cut to just the roi region.
% Any pixels not falling into the roi are set to zero in all channels.
    roi_corners = [corners(1,:); corners(2,:); corners(4,:); corners(3,:)];
    roi = images.roi.Polygon;
    roi.Position = roi_corners;
    mask = createMask(roi, img);
    masked_img = bsxfun(@times, img, cast(mask, 'like', img));
    masked_img(all(~mask,2),:,:) = [];
    masked_img(:,all(~mask,1),:) = [];
    cutout = masked_img;
end

