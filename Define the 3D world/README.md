# 3D reconstruction


**main**: Put your image in variable image_path, change name of your output image in save_as and run.

# Functions to Create 3D World

* **get_5rectangles**: Vertices of 5 walls are extracted.
* **points_3d**: 3D world coordinates of 12 points described in the paper are created.
* **get_back_image**: Rear wall from image is extracted.
* **keystone**: Left wall, right wall, ceiling and floor images are extracted in rectangular form.
* **plot_3D**: 3D model is created using extracted walls and coordinates of 12 points.

# Functions to Change View in 3D Model

* **change_camera_position**: CameraPosition in [x y z] is changed.
* **tilt_figure**:            CameraUpVector in [x y z] is changed.
* **rotate_figure**:          CameraTarget in [x y z] is changed.
* **zoom_in_figure**:         CameraViewAngle is changed.

# Save New 3D Model as Image

**save_as**
