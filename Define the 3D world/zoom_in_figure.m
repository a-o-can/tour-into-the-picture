function zoom_in_figure(model, magnitude)
    % This function zooms in the image.
    % Example:
    %   zoom_in_figure(model, 60)
    tmp = model.CameraViewAngle;
    model.CameraViewAngle = tmp - magnitude;
end