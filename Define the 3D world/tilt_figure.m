
function tilt_figure(model, axis, degree)
    % This function tilts the image by the given axis.
    % Example:
    %   tilt_figure(model, "x", 3)
    tmp = model.CameraUpVector;
    x = tmp(1);
    y = tmp(2);
    z = tmp(3);
    if axis == "x"
        x = x + degree;
    elseif axis == "y"
        y = y + degree;
    elseif axis == "z"
        z = z + degree;
    end
    model.CameraUpVector = [x y z];
end